from extension import db


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column('user_id', db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(30), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)
    name = db.Column(db.String(30), nullable=False)
    role_name = db.Column(db.String(30),  nullable=False)
    createtime = db.Column(db.Time)


class Run(db.Model):
    __tablename__ = 'runs'

    id = db.Column('run_id', db.Integer, primary_key=True, autoincrement=True)

    user_id = db.Column(db.Integer, db.ForeignKey('users.user_id'))

    date = db.Column(db.DateTime, nullable=False)
    time = db.Column(db.Integer, nullable=False, default=0)  # Measured in Minutes
    distance = db.Column(db.Float, nullable=False, default=0.0)  # Mesured in KM s
    longitude = db.Column(db.Float, nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    weather_type = db.Column(db.String, nullable=True, default=None)
    temperature = db.Column(db.Float, nullable=True, default=None)

