"""empty message

Revision ID: 211ec774e6e1
Revises: 
Create Date: 2020-01-29 19:31:12.698120

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '211ec774e6e1'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('users')
    op.drop_table('roles')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('roles',
    sa.Column('id', sa.INTEGER(), server_default=sa.text(u"nextval('roles_id_seq'::regclass)"), autoincrement=True, nullable=False),
    sa.Column('name', sa.VARCHAR(length=120), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('id', name=u'roles_pkey'),
    sa.UniqueConstraint('name', name=u'roles_name_key'),
    postgresql_ignore_search_path=False
    )
    op.create_table('users',
    sa.Column('user_id', sa.INTEGER(), autoincrement=True, nullable=False),
    sa.Column('username', sa.VARCHAR(length=30), autoincrement=False, nullable=False),
    sa.Column('password', sa.VARCHAR(length=120), autoincrement=False, nullable=False),
    sa.Column('name', sa.VARCHAR(length=30), autoincrement=False, nullable=False),
    sa.Column('role_id', sa.INTEGER(), autoincrement=False, nullable=False),
    sa.Column('createtime', postgresql.TIME(), autoincrement=False, nullable=True),
    sa.ForeignKeyConstraint(['role_id'], [u'roles.id'], name=u'users_role_id_fkey'),
    sa.PrimaryKeyConstraint('user_id', name=u'users_pkey'),
    sa.UniqueConstraint('username', name=u'users_username_key')
    )
    # ### end Alembic commands ###
