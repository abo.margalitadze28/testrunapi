import json

from flask import Blueprint
from flask import request
from datetime import datetime

from flask_jwt_extended import (

    create_refresh_token,
    create_access_token
)

from models import User
from extension import db

user_reg = Blueprint('user', __name__)


@user_reg.route('/Registratin', methods=['POST'])
def add_user():
    name = request.json['name']
    username = request.json['username']
    password = request.json['password']
    time = datetime.now()
    role_name = request.json['role_name']
    new_user = User(name=name, username=username, password=password, role_name=role_name, createtime=time)

    db.session.add(new_user)
    db.session.commit()

    access_token = create_access_token(identity=username)
    refresh_token = create_refresh_token(identity=username)

    result = {
        "id": new_user.id,
        "name": name,
        "username": username,
        "time": str(time),
        "msg": "account already created please log in ",
        "access_token": access_token,
        "refresh_token":refresh_token
    }


    return json.dumps(result)
