from flask_login import LoginManager

from extension import db, jwt
from flaskapp import app
from user_reg.register import user_reg
from user_login.loggin import loggin
from user_run.run import run_user
from admin_users.admin_user import admin_user
from user_API.user_api import user_api

def create_app():
    db.init_app(app)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    with app.app_context():
        db.create_all()

    app.register_blueprint(loggin)
    app.register_blueprint(user_reg)
    app.register_blueprint(run_user)
    app.register_blueprint(admin_user)
    app.register_blueprint(user_api)

    jwt.init_app(app)
    return app


if __name__ == '__main__':
    app = create_app()
    app.run(port=5001)



