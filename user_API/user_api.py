import json

from datetime import datetime, timedelta

from flask import Blueprint
from flask import request
from flask_jwt_extended import (

    get_jwt_identity

)
from helper_func.requireds import login_required
from models import Run

user_api = Blueprint('user_api', __name__)


@user_api.route('/user/speed', methods=['GET'])
@login_required()
def get_users_speed():
    time_per_day = int(request.args.get('time_per_day'))

    d = datetime.today() - timedelta(days=time_per_day)
    current_user = get_jwt_identity()
    average_speed = 0
    distance = 0
    time = 0
    user_runs = Run.query.filter(Run.date >= d)
    for run in user_runs:
        if run.user_id == current_user:
            distance += run.distance
            time += run.time
    average_speed = distance / (time * user_runs.count())

    return {
        "users average speed in this time is :":
            average_speed
    }


@user_api.route('/user/distance', methods=['GET'])
@login_required()
def get_user_distance():
    time_per_day = int(request.args.get('time_per_day'))

    d = datetime.today() - timedelta(days=time_per_day)
    current_user = get_jwt_identity()
    distance = 0
    user_runs = Run.query.filter(Run.date >= d)
    for run in user_runs:
        if run.user_id == current_user:
            distance += run.distance

    return {
        "you walking  distance  is :":
            distance
    }


@user_api.route('/user/distance/all', methods=['GET'])
@login_required()
def get_all_distance():
    periode = request.args.get('periode')
    current_user = get_jwt_identity()
    user_runs = Run.query.filter_by(user_id=current_user)
    distance_per_day = []
    distance = 0

    if periode == "day":
        day = 1

        u = datetime.today()
        for run in user_runs:
            d = datetime.today() - timedelta(days=day)
            user_run = Run.query.filter(Run.date >= d, Run.date <= u).all()
            distance_per_day.append(user_run)
            day = Run.date
            u = Run.date



    return {
        "you walking  distance  is :":
            distance
    }


@user_api.route('/user/search', methods=['GET'])
def user_search():
    periode = request.args
    distance=request.args.getlist('distance')
    weather_type = request.args.getlist('weather_type')
    return {
        "you walking  distance  is :"

    }