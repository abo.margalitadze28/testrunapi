import json

from flask import Blueprint
from flask import make_response
from flask import request
from flask_jwt_extended import (
    jwt_refresh_token_required,
    get_jwt_identity,
    create_refresh_token,
    create_access_token,
    jwt_required
)

from helper_func.requireds import login_user
from models import User

loggin = Blueprint('loggin', __name__)



@loggin.route('/login', methods=['GET'])
def login():
    username = request.json['username']
    password = request.json['password']

    if not username or not password:
        return make_response('Could not verify  aaa ', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

    user = User.query.filter_by(username=username).first()

    if not user:
        return make_response('Could not verify eee', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

    if user and user.password == password:
        print("oeee iuzgar ")
        access_token, refresh_token = login_user(user.id)
        return {
            'message': 'User {} was logged in'.format(username),
            'access_token': access_token,
            'refresh_token': refresh_token
        }

    return make_response('Could not verify oooo  ', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})



@loggin.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def TokenRefresh():
    current_user = get_jwt_identity()
    access_token = create_access_token(identity=current_user)
    return {'access_token': access_token}
