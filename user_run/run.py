import json

import requests
from flask import Blueprint
from flask import request
from datetime import datetime

from flask_jwt_extended import (

    get_jwt_identity

)

from helper_func.requireds import login_required, requires_roles
from user_run.conventer import run_to_json
from models import Run
from extension import db

run_user = Blueprint('run_user', __name__)


@run_user.route('/run', methods=['POST'])
@login_required()
@requires_roles('user')

def add_run():
    user_id = request.json['user_id']
    time = request.json['time']
    distance = request.json['distance']
    longitude = request.json["longitude"]
    latitude = request.json['latitude']

    url = "https://api.openweathermap.org/data/2.5/weather"
    location = "?lat=" + latitude + "&lon=" + longitude + "&appid=ab95eee668777724991c91e987322e9a"
    url += location
    response = requests.get(url)

    data = response.json()

    weather_type = data["weather"][0]["main"]
    temperature = data["main"]["temp"]
    date = datetime.now()
    run = Run(user_id=user_id, time=time, distance=distance, longitude=longitude, latitude=latitude,
              weather_type=weather_type, temperature=temperature, date=date)

    db.session.add(run)
    db.session.commit()

    return {
        "message": "Run {} has been successfully created".format(run.id),
        "run_info": run_to_json(run)
    }


@run_user.route('/run/,<id>', methods=['PUT'])
@login_required()
@requires_roles('user')


def put_run(id):
    run = Run.query.get(id)
    time = request.json['time']
    distance = request.json['distance']
    longitude = request.json["longitude"]
    latitude = request.json['latitude']

    weather_type = request.json['weather_type']
    temperature = request.json['temperature']
    date = datetime.now()

    run.time = time
    run.distance = distance
    run.longitude = longitude
    run.latitude = latitude
    run.weather_type = weather_type
    run.temperature = temperature
    run.date = date
    db.session.commit()

    return {
        "message": "Run {} has been successfully change".format(run.id),
        "run_info": run_to_json(run)
    }


@run_user.route('/run/<id>', methods=['GET'])
@login_required()
@requires_roles('user')


def get_run_id(id):
    run = Run.query.get(id)

    return json.dumps(run_to_json(run))


@run_user.route('/run', methods=['GET'])
@login_required()
@requires_roles('user')


def get_run_list():
    current_user = get_jwt_identity()
    run = Run.query.get(user_id=current_user)
    result = list(map(run_to_json, run))

    return json.dumps(result)


@run_user.route('/run/<id>', methods=['DELETE'])
@login_required()
@requires_roles('user')


def delete_run_id(id):
    run = Run.query.get(id)
    db.session.delete(run)
    db.session.commit()
    return {

        "run is deleted "
    }
