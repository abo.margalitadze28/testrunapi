
def run_to_json(run):
    return {
        "run_id": run.id,
        "user_id": run.user_id,
        "date": str(run.date),
        "time": run.time,
        "distance": run.distance,
        "longitude": run.longitude,
        "latitude": run.latitude,
        "weather_type": run.weather_type,
        "temperature": run.temperature
    }