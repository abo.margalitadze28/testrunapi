from functools import wraps

from flask_jwt_extended import (
    jwt_refresh_token_required,
    get_jwt_identity,
    create_refresh_token,
    create_access_token,
    jwt_required
)

from models import User


def login_required():
    def wrapper(f):
        @wraps(f)
        @jwt_required
        def wrapped(*args, **kwargs):
            id = get_jwt_identity()
            if User.query.get(id):
                return f(*args, **kwargs)
            return {'message': 'Please log in to continue'}, 401

        return wrapped

    return wrapper


def login_user(username):
    access_token = create_access_token(identity=username)
    refresh_token = create_refresh_token(identity=username)
    return access_token, refresh_token



def requires_roles(*roles):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            if get_current_user_role() not in roles:
                return "Message:Forbidden"
            return f(*args, **kwargs)

        return wrapped

    return wrapper




def get_user_role_name(user_id):
    user=User.query.filter_by(id=user_id).first()
    userrole=user.role_name
    return userrole


def get_current_user_role():
    return get_user_role_name(get_jwt_identity())
