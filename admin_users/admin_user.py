import json

import requests
from flask import Blueprint
from flask import request
from datetime import datetime

from admin_users.converters import user_to_json
from helper_func.requireds import login_required, requires_roles
from models import User

admin_user = Blueprint('admin_user', __name__)


@admin_user.route('/admin/users', methods=['GET'])
@login_required()
@requires_roles('admin')
def get_all_users():

          size = request.args.get('size')
          index = request.args.get('index')

          all_users = User.query.limit(size).offset(size * index).all()

          result = list(map(user_to_json, all_users))

          return json.dumps(result)



